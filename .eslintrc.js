// To solve Parsing error: "parserOptions.project" has been set for @typescript-eslint/parser.
// The file does not match your project config: common/util/new.ts.
// https://stackoverflow.com/questions/58510287/parseroptions-project-has-been-set-for-typescript-eslint-parser

// The file must be included in at least one of the projects provided.

//  Add 
//   1. below in parserOptions the following: project: ["./tsconfig.eslint.json"],
//   2. Create tsconfig.eslint.json -file at the root with the following content:
//   {
//     "extends": "./tsconfig.json",
//     "include": [
//       "src/**/*.ts",
//       ".eslintrc.js",
//       "tests/**/*.ts",
//     ],
//     "exclude": ["node_modules"]
//   }

// Or alternatively just add the following into tsconfig.json:

//       "src/**/*.ts",
//       ".eslintrc.js",
//       "tests/**/*.ts",
//     ],

module.exports = {
  parser: '@typescript-eslint/parser',
  parserOptions: {
    project: './tsconfig.json',
    sourceType: 'module',
  },
  plugins: ['@typescript-eslint/eslint-plugin'],
  extends: [
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:@typescript-eslint/recommended',
    'prettier',
    'prettier/@typescript-eslint',
  ],
  root: true,
  env: {
    node: true,
    jest: true,
  },
  rules: {
    '@typescript-eslint/interface-name-prefix': 'off',
    '@typescript-eslint/explicit-function-return-type': 'off',
    '@typescript-eslint/no-explicit-any': 'off',
  },
};
