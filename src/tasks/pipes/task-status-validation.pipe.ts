import { PipeTransform, ArgumentMetadata, BadRequestException } from '@nestjs/common';
import { TaskStatus } from '../task.interface';

export class TaskStatusValidationPipe implements PipeTransform {
  readonly allowedStatuses = [
    TaskStatus.OPEN,
    TaskStatus.IN_PROGRESS,
    TaskStatus.DONE,
  ];

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  transform(value: any, metadata: ArgumentMetadata) {
    // console.log('value' + value);
     console.log('metadata' + metadata);
    // indexOf return -1 if status not found
    value = value.toUpperCase();

    if (!this.isStatusValid(value)) {
        throw new BadRequestException(`Invalid status value '${value}' was provided`);
    }

    return value;
  }

  private isStatusValid(value: any): any {
    const inx = this.allowedStatuses.indexOf(value);
    // return only if inx is uneqaul to -1
    return inx !== -1;
  }
}
