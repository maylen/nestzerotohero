import { Entity, BaseEntity, PrimaryGeneratedColumn, Column } from "typeorm";
import { TaskStatus } from "./task.interface";

@Entity()
export class Task extends BaseEntity  {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;
    
    @Column()
    description: string;

    @Column()
    status: TaskStatus;
}