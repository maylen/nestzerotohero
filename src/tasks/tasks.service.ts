import { Injectable, NotFoundException } from '@nestjs/common';
import { Task, TaskStatus } from './task.interface';
import { v4 as uuidv4 } from 'uuid'; // https://www.npmjs.com/package/uuid
import { CreateTaskDto } from './dto/create-task.dto';
import { GetTaskFilterDto } from './dto/get-tasks-filter.dto';

@Injectable()
export class TasksService {
  // private here means that tasks array is only available for
  // TaskComponent and no other component which injects this
  // service can mutate this array
  private tasks = [];

  getAllTasks(): Task[] {
    return this.tasks;
  }

  getTasksWithFilters(filterDto: GetTaskFilterDto): Task[] {
    const { search, status } = filterDto;

    let tasks = this.getAllTasks();

    if (status) {
      tasks = tasks.filter((task: Task) => task.status === status);
    }

    if (search) {
      tasks = tasks.filter(
        (task: Task) =>
          task.title.includes(search) || task.description.includes(search),
      );
    }

    return tasks;
  }

  //createTask(title: string, description: string): Task {
  createTask(createTaskDto: CreateTaskDto): Task {
    // picks only the properties given in brackets (es6 syntax for object destructuring)!!!
    const { title, description } = createTaskDto;
    const task: Task = {
      id: uuidv4(),
      title,
      description,
      status: TaskStatus.OPEN,
    };
    this.tasks.push(task);
    return task;
  }

  getTaskById(id: string): Task {
    const found: Task = this.tasks.find((task: Task) => task.id === id);
    if (!found) {
        throw new NotFoundException(`NotFoundException: Task id ${id} is not found`); 
    }
    return found;
  }

  deleteTaskById(id: string): Task {
    // note: uses uses the id-validation via getTaskById
    const task = this.getTaskById(id);
    this.tasks = this.tasks.filter((task: Task) => task.id === task.id);
    // const taskIndex = this.tasks.findIndex((task: Task) => task.id === id);
    // if (taskIndex !== -1) {
    //   const task = this.tasks[taskIndex];
    //   this.tasks.splice(taskIndex, 1);
    //   return task;
    // } else {
    //   return `Task not found by id ${id}`;
    // }
    return task;
  }

  updateStatus(id: string, status: TaskStatus): Task {
    // note: uses uses the id-validation via getTaskById
    const task: Task = this.getTaskById(id);
    task.status = status;
    return task;
  }
}
